const GameEngine = require("../lib/game_engine");

describe("The Game Engine must", () => {
  test("start a new game with a random word", () => {
	  
	  const game_state = GameEngine.startGame();
	  expect(game_state.lives).toBe(5);
	  expect(game_state.word.length).toBeGreaterThan(3);
	  expect(game_state.status).toBe("RUNNING");
	  expect(game_state.guesses.length).toBe(0);
	  expect(game_state.display_word.length).toBe(game_state.word.length*2-1);
	  expect(game_state.display_word.split(" ").length).toBe(game_state.word.length);
	  expect(game_state.display_word.split(" ").every( v => v === "_" )).toBe(true);
	  
  });

  test("update the guesses and lives when a wrong guess is given", () => {});
});
