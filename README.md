# The Game Engine

The game engine is a simple machine. It may start a game or compute a guess, and
it will always return a `game_status`:

```
{
  status: RUNNING,
  word: 'banana',
  guesses: ['a', 'b', 'c', 'd'],
  display_word: 'b a _ a _ a',
  lives: 3 // Initial 5 - 2 wrong guesses
}
```

# 💡 Exercise 1: Packages and Build Automation 💡

Add the package you just created to your `package.json` file and try it! Well,
if you did everything right, a strange error will appear.

Fix this error (you might need to change the **random_line** package), and
update your `package.json` if needed.

# 💡 Exercise 2: TDD, Code Coverage and Real Time Development 💡

Now it's time to finish the game engine. But now we're getting pretty picky with
code coverage, and your implementation must adhere to our policy.

While you're at it, try to add a new script to your `package.json` file using
the options `watch` and `notify` from jest. You can check the
[documentation here](https://jestjs.io/docs/en/cli#watch). This way you're going
to try **REAL TIME DEVELOPMENT**!

Go for it!

# 💡 Exercise 3: Continuous Integration and Continuous Delivery 💡

The whole CI magic happens in the file `.gitlab-ci.yml`. It's already testing,
but it could be publishing your new library at the end of the pipeline. Try
adding a new step named `deploy` and running `npm publish --access public` on
it.

Oh, and don't forget to increment your version before doing that, or you'll
receive an error.

TIP: Instead of changing your `package.json` file, try the following command:
`npm version patch`
