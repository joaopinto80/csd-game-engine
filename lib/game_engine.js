const random_line = require("@jp80/random_line");

const startGame = () => {
	var braveNewWord = random_line.getRandomWord();
	var displayWord = getDisplayWord(braveNewWord);
  return {
    status: "RUNNING",
    word: braveNewWord,
    lives: 5,
    display_word:  displayWord,
    guesses: []
  };
};

const getDisplayWord = (braveNewWord) => {

	return Array(braveNewWord.length).fill("_").join(" ") ;
};

const takeGuess = (game_state, guess) => {
  return {
    ...game_state,
    lives: game_state.lives - 1
  };
};

module.exports = {
  startGame,
  takeGuess
};
